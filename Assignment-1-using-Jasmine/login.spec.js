var Login = require("./login-page");

describe("Login Page", function() {

   var login = new Login();

   it("Check if credentials are valid", function() {
      expect(login.login("nav",123)).toBe(true);
   });

   it("Check if credentials are valid", function() {
      expect(login.login("navaaa",12333)).toBe(false);
   });

  });