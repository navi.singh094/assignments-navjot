import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  user = { userName: '', password: '' };

  constructor(public router: Router) {}

  ngOnInit(): void {}

  goToDashboard() {
    if (this.username == '' || this.password == '') {
      document.getElementById('authenticationmsg').innerHTML =
        'Username and password are required';
    } else if (this.username == 'nav' && this.password == '123') {
      this.router.navigate(['dashboard']);
    } else
      document.getElementById('authenticationmsg').innerHTML =
        'Invalid Credentials';
  }
}
