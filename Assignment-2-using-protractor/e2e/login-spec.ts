import { browser, element, by } from 'protractor';

describe('Login Page', function () {

  it('Blank Credentials', function () {
    browser.get('http://localhost:4201');

    element(by.id('username')).sendKeys('');
    element(by.id('password')).sendKeys('');

    element(by.id('submit')).click();
    browser.driver.sleep(2000);
    browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Username and password are required');
      });
  });

  it('InValid Credentials', function () {
    browser.get('http://localhost:4201');

    element(by.id('username')).sendKeys('nav');
    element(by.id('password')).sendKeys('nav');

    element(by.id('submit')).click();
    browser.driver.sleep(2000);
    browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Invalid Credentials');
      });
  });

  it('Valid Credentials', function () {
    browser.get('http://localhost:4201');

    element(by.id('username')).sendKeys('nav');
    element(by.id('password')).sendKeys('123');

    element(by.id('submit')).click();
    browser.driver.sleep(2000);
    browser.waitForAngular();
    element(by.id('dashboard'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Welcome To Dashboard Page');
      });
  });
});
